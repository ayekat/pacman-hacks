if [ -z "${_PH_PACMAN_SH_:-}" ]; then readonly _PH_PACMAN_SH_=1

readonly PACMAN_CONFIG=/etc/pacman.conf # overwritten by Makefile
readonly PACMAN_DBPATH=$(pacman-conf --config "$PACMAN_CONFIG" DBPath)

pacman_config=$PACMAN_CONFIG

pacman_set_dbpath()
{
	if [ ! -d "$1" ]; then
		die $E_FILESYSTEM '%s: no such directory' "$1"
	fi
	pacman_config=$XDG_RUNTIME_DIR/$PHCMD/pacman.conf
	mkdir -p "$(dirname "$pacman_config")"
	pacman-conf | sed -e "s|^\\(DBPath = \\).*\$|\\1$1|g" \
		>"$pacman_config"
}

_expac() { expac --config "$pacman_config" "$@"; }
_pacman_conf() { pacman-conf --config "$pacman_config" "$@"; }

fi
