if [ -z "${_PH_PACKAGE_SH_:-}" ]; then readonly _PH_PACKAGE_SH_=1

scan_pkgname()
{
	case "$1" in (*-*-*) ;; (*)
		false; return
	esac

	pkgrel=${pkgname##*-}
	pkgname=${pkgname%-$pkgrel}
	pkgver=${pkgname##*-}
	pkgname=${pkgname%-$pkgver}
}

fi
