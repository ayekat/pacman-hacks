.TH REPKG 1 @@DATE@@ "pacman-hacks @@VERSION@@" "user commands"

.\" disable hyphenation, left-align
.nh
.ad l

.\"=============================================================================
.SH NAME

repkg - apply changes to pacman packages

.\"=============================================================================
.SH SYNOPSIS

\fBrepkg -h\fR
.br
\fBrepkg -v\fR
.br
\fBrepkg\fR [\fIoption\fR...]

.\"=============================================================================
.SH DESCRIPTION

repkg takes a given pacman package file, applies changes to it according to a
configuration file, and saves the result as a new package file. Possible
modifications are changing metadata (adding/removing hard and optional
dependencies, provisions, conflicts, replacements, backups and groups; setting
the URL, description, and the architecture; renaming the package),
creating/renaming/deleting files, changing file permissions and ownership, and
patching text files.

It was written to allow users to easily apply custom changes to official
packages without having to adopt the full-blown package maintenance work. It
does not substitute a properly built package from a PKGBUILD (e.g. anything
related to compilation flags can likely not be fixed), but it should be able to
fix most issues related to package metadata.

.SS Options

Mandatory options are: \fB-i\fR, \fB-r\fR.

.TP 4
\fB-b\fR \fIdbpath\fR
Use pacman package database in \fIdbpath\fR. The default is the location
specified in \fBpacman.conf\fR(5).

.TP
\fB-d\fR
Display debug messages.

.TP
\fB-f\fR
Overwrite output package file if it already exists.

.TP
\fB-h\fR
Display a help message and exit.

.TP
\fB-i\fR \fIfile\fR
Repackage "input" package \fIfile\fR.

.TP
\fB-k\fR
Keep a copy of the modified \fB.MTREE\fR, \fB.PKGINFO\fR and \fB.BUILDINFO\fR.

.TP
\fB-o\fR \fIdir\fR
Save resulting ("output") package file in \fIdir\fR. By default, the package is
saved in the current working directory.

.TP
\fB-r\fR \fIfile\fR
Use \fIfile\fR as rules file.

.TP
\fB-R\fR \fIrepkgrel\fR
The repackaging release number. This is appended with a dot to the original
package release number (pkgrel), as a way to both distinguish repackaged
packages from their original and allow multiple properly versioned rebuilds from
the same original package. By default, this is \fB1\fR (e.g. \fBfoobar-1.2-3\fR
becomes \fBfoobar-1.2-3.1\fR).

.TP
\fB-v\fR
Print version and licence info and exit.

.\"=============================================================================
.SH CONFIGURATION

.SS Rules

repkg applies changes according to so-called "rules", described a rules file
passed with \fB-r\fR. Each rule is written on a separate line and has the
following format:

.RS 4
.EX
\fIcommand\fR \fIargument\fR...
.EE
.RE

For example, the following two rules will modify a package and turn its hard
dependency \fBfoobar\fR into an optional dependency:

.RS 4
.EX
remove-depend foobar
add-optdepend foobar: for fooing bars
.EE
.RE

Lines starting with \fB#\fR are considered comments and thus ignored. Empty
lines are ignored, too.

If an illegal rule is encountered, metapkg aborts. A rule may be "illegal"
e.g. for containing syntax errors, using an unknown command, or invalid
arguments passed to a command.

Available commands are listed and described in the \fBCOMMANDS\fR section below.

.SS Patches

repkg can modify text files by using \fBpatch\fR(1). The command for applying a
patch is \fBapply\fR:

.RS 4
.EX
apply superfoo-fix-frobnicate.diff /etc/superfoo.conf
.EE
.RE

Patch files must apply changes to exactly one file.

.\"=============================================================================
.SH COMMANDS

This section lists the complete list of available rule files commands and
arguments they take. All paths \fIpath\fR (and \fInewpath\fR) must be absolute
paths.

.SS Metadata commands

These commands modify the package metadata:

.TP 4
\fBset-name \fIpkgname\fR
Set package name to \fIpkgname\fR. This will cause repkg not to add a provision
for the original package name and version, as this is now a differently named
package. The output file is adapted accordingly.

.TP
\fBset-arch \fIarch\fR
Set package architecture to \fIarch\fR.

.TP
\fBset-desc \fIpkgdesc\fR
Set package description to \fIpkgdesc\fR. \fIpkgdesc\fR may consist of multiple
words.

.TP
\fBset-url \fIurl\fR
Set package upstream URL to \fIurl\fR.

.TP
\fBadd-depend \fIpackage\fR
Add dependency on \fIpackage\fR.

.TP
\fBremove-depend \fIpackage\fR
Remove dependency on \fIpackage\fR.

.TP
\fBadd-optdepend \fIpackage\fB: \fIdescription\fB
Add optional dependency on \fIpackage\fR, with an "optional dependency reason"
\fIdescription\fR. \fIdescription\fR may consist of multiple words.

.TP
\fBremove-optdepend \fIpackage\fR
Remove optional dependency on \fIpackage\fR.

.TP
\fBadd-replace \fIpackage\fR
Add \fIpackage\fR to be replaced.

.TP
\fBremove-replace \fIpackage\fR
Remove \fIpackage\fR to be replaced.

.TP
\fBadd-conflict \fIpackage\fR
Add \fIpackage\fR as conflict.

.TP
\fBremove-conflict \fIpackage\fR
Remove \fIpackage\fR as conflict.

.TP
\fBadd-provide \fIpackage\fR
Add provision for \fIpackage\fR.

.TP
\fBremove-provide \fIpackage\fR
Remove provision for \fIpackage\fR.

.TP
\fBadd-backup \fIpath\fR
Add file \fIpath\fR as backup file.

.TP
\fBremove-backup \fIpath\fR
Remove file \fIpath\fR as backup file.

.TP
\fBadd-group \fIgroup\fR
Add package to \fIgroup\fR.

.TP
\fBremove-group \fIgroup\fR
Remove package from \fIgroup\fR.

.SS File commands

These commands modify files in the package:

.TP 4
\fBcreate-file \fIpath\fR
Create empty file \fIpath\fR, with mode 644, UID 0 and GID 0.

.TP
\fBcreate-link \fIpath\fR \fItarget\fR
Create symbolic link \fIpath\fR linking to \fItarget\fR, with mode 777, UID 0
and GID 0.

.TP
\fBcreate-dir \fIpath\fR
Create directory \fIpath\fR, with mode 755, UID 0 and GID 0.

.TP
\fBadd-file \fIfile\fR \fIpath\fR
Add locally provided file \fIfile\fR as \fIpath\fR, with mode 644, UID 0 and GID
0. \fIfile\fR may be a relative or absolute path.

.TP
\fBchange-file-uid \fIpath\fR \fIuid\fR
Change ownership for file or directory \fIpath\fR to UID \fIuid\fR.

.TP
\fBchange-file-gid \fIpath\fR \fIgid\fR
Change group ownership for file or directory \fIpath\fR to GID \fIgid\fR.

.TP
\fBchange-file-mode \fIpath\fR \fImode\fR
Change access permissions for file or directory \fIpath\fR to
\fImode\fR. \fImode\fR must be in octal as described in \fBchmod\fR(1).

.TP
\fBrename-file \fIpath\fR \fInewpath\fR
Rename file or directory \fIpath\fR to \fInewpath\fR. \fInewpath\fR must not
exist, but all directories leading up to \fInewpath\fR must exist.

.TP
\fBcopy-file \fIpath\fR \fInewpath\fR
Copy non-directory file \fIpath\fR to \fInewpath\fR. \fInewpath\fR must not
exist, but all directories leading up to \fInewpath\fR must exist.

.TP
\fBdelete-file \fIpath\fR
Delete file or empty directory \fIpath\fR.

.TP
\fBdelete-dir \fIpath\fR
Delete empty or non-empty directory \fIpath\fR and all contained files or
directories recursively.

.TP
\fBapply \fIpatch\fR \fIpath\fR
Apply locally provided patch file \fIpatch\fR on regular file
\fIpath\fR. \fIpatch\fR may be a relative or absolute path. See the
\fBPatches\fR subsection above for more details.

.SS Verification commands

These commands do not modify the package, but merely perform checks, and will
error out if a check fails:

.TP 4
\fBexpect-name \fIpkgname\fR
Verify that the package name is \fIpkgname\fR.

.TP
\fBexpect-arch \fIarch\fR
Verify that the package architecture is \fIarch\fR.

.TP
\fBexpect-desc \fIpkgdesc\fR
Verify that the package description is \fIpkgdesc\fR. \fIpkgdesc\fR may consist
of multiple words.

.TP
\fBexpect-url \fIurl\fR
Verify that the package upstream URL is \fIurl\fR.

.TP
\fBexpect-depend \fIpackage\fR
Verify that the package depends on \fIpackage\fR.

.TP
\fBexpect-nodepend \fIpackage\fR
Verify that the package does not depend on \fIpackage\fR.

.TP
\fBexpect-optdepend \fIpackage\fR
Verify that the package optionally depends on \fIpackage\fR.

.TP
\fBexpect-nooptdepend \fIpackage\fR
Verify that the package does not optionally depend on \fIpackage\fR.

.TP
\fBexpect-replace \fIpackage\fR
Verify that the package replaces \fIpackage\fR.

.TP
\fBexpect-noreplace \fIpackage\fR
Verify that the package does not replace \fIpackage\fR.

.TP
\fBexpect-conflict \fIpackage\fR
Verify that the package conflicts with \fIpackage\fR.

.TP
\fBexpect-noconflict \fIpackage\fR
Verify that the package does not conflict with \fIpackage\fR.

.TP
\fBexpect-provide \fIpackage\fR
Verify that the package provides \fIpackage\fR.

.TP
\fBexpect-noprovide \fIpackage\fR
Verify that the package does not provide \fIpackage\fR.

.TP
\fBexpect-group \fIpackage\fR
Verify that the package provides \fIpackage\fR.

.TP
\fBexpect-nogroup \fIpackage\fR
Verify that the package does not provide \fIpackage\fR.

.TP
\fBexpect-backup \fIpath\fR
Verify that the package backs up \fIpath\fR.

.TP
\fBexpect-nobackup \fIpath\fR
Verify that the package does not back up \fIpath\fR.

.TP
\fBexpect-file-uid \fIpath\fR [\fIuid\fR]
Verify user ownership of file or directory \fIpath\fR (compare the UID set in
\fB.MTREE\fR with the actual UID in the filesystem). If \fIuid\fR is given, also
verify that the file is owned by UID \fIuid\fR.

.TP
\fBexpect-file-gid \fIpath\fR [\fIgid\fR]
Verify group ownership of file or directory \fIpath\fR (compare the GID set in
\fB.MTREE\fR with the actual GID in the filesystem). If \fIgid\fR is given, also
verify that the file is owned by GID \fIuid\fR.

.TP
\fBexpect-file-mode \fIpath\fR [\fImode\fR]
Verify access permissions of file or directory \fIpath\fR (compare the mode set
in \fB.MTREE\fR with the actual mode in the filesystem). If \fImode\fR is given,
also verify that the file's mode is \fImode\fR. \fImode\fR must be given in
octal as described in \fBchmod\fR(1).

.TP
\fBexpect-file-sha256 \fIpath\fR [\fIhash\fR]
Verify the SHA256 checksum of file or directory \fIpath\fR (compare the checksum
set in \fB.MTREE\fR with the actual file's checksum in the filesystem). If
\fIhash\fR is given, also verify that the checksum is \fIhash\fR.

.\"=============================================================================
.SH LIMITATIONS

Given that the original package has to be available, and command line options
can be rather cumbersome to type everytime, repkg itself is unsuited for direct
use. Users are encoured to use \fBremakepkg\fR(1) instead, a wrapper script that
handles the downloading of the package from the package mirror as well with
so-called REPKGBUILD files.

Apart from usability limitations, there are also functional limitations:

.IP " \(bu" 4
The output package is compressed with \fBzstd\fR(1), regardless of the settings
in \fBmakepkg.conf\fR(8).

.IP " \(bu"
Paths currently cannot contain spaces, tabs or newlines.

.\"=============================================================================
.SH SEE ALSO

\fBremakepkg\fR(1)

.\"=============================================================================
.SH BUGS

Please report bugs on https://gitlab.com/ayekat/pacman-hacks/issues. I'm just a
hobby developer, I promise I won't bite!

.\"=============================================================================
.SH AUTHOR

repkg was written by Tinu "ayekat" Weber on a rather cloudy, cold Sunday in
February 2018.
